<?php
use Bitrix\Main\Type\DateTime;
class ChatBot
{
    private int $userId;
    private ?int $questionId = null;
    private ?int $nextQuestionId = null;
    private int $scenario;
    private int $scenarioMain;
    private int $attemptId;
    private ?int $answerId = null;
    private int $answerRowId;
    private bool $isFirstQuestion = false;

    const STATUS_SUCCESS = 'CORRECT';
    const STATUS_ACCEPTABLE = 'ACCEPTABLE';
    const STATUS_WRONG = 'WRONG';

    const LVL_JUNIOR = 'junior';
    const LVL_MIDDLE = 'middle';
    const LVL_SENIOR = 'senior';

    const DEFAULT_ATTEMPT_LIMIT = 3;

    public static array $_ids = [
        self::STATUS_SUCCESS => 1,
        self::STATUS_ACCEPTABLE => 2,
        self::STATUS_WRONG => 3,
    ];

    public static array $_lvl = [
        self::LVL_JUNIOR => 1,
        self::LVL_MIDDLE => 2,
        self::LVL_SENIOR => 3,
    ];

    public static array $_lvlRank = [
         1 => self::LVL_JUNIOR,
         2 => self::LVL_MIDDLE,
         3 => self::LVL_SENIOR,
    ];

    public static array $_messages = [
        1 => "обратитесь за помощью к наставнику",
        2 => "попробуйте повторить материал и возвращайтесь",
        3 => "хороший результат. попробуйте еще раз"
    ];

    /**
     * @return array
     */
    public function actionGetQuestion(): array
    {
        return $this->getAttempt()->getQuestion();
    }

    /**
     * @return array
     */
    public function actionSetAnswer(): array
    {
        $isUpdate = $this->getAttempt()->updateQuestionResult();
        return $isUpdate ? $this->getQuestion() : [];
    }

    /**
     * @return array
     */
    public function actionFinish(): array
    {
        $result = [];
        $isUpdate = $this->getAttempt()->updateQuestionResult();
        if($isUpdate){
            $resultTotal = $this->updateAttempt();
            $result['total'] = $resultTotal;
            $result['status'] = true;
            $result['actions'] = $this->getAnswerActions();
            $result['percent'] = $this->percentagePassed();
            $result['message'] = $result['actions']['finish'] ?: '';
            if($result['percent'] <= 50) {
                $result['message'] = self::$_messages[1];
            } elseif ($result['percent'] > 50 && $result['percent'] <= 80) {
                $result['message'] = self::$_messages[2];
            } elseif ($result['percent'] > 80 && $result['percent'] < 100) {
                $result['message'] = self::$_messages[3];
            }
            if ($resultTotal == self::$_ids[self::STATUS_SUCCESS]) {
                $this->updateLevel();
            }
        }
        return $result;
    }

    /**
     * @return bool
     */
    public function actionAnew(): bool
    {
        $isDeleteQuestionRow = $this->deleteBeforeAnew();
        return $isDeleteQuestionRow && $this->closeAttempt();
    }

    /**
     * @return ChatBot
     */
    private function getAttempt(): ChatBot
    {
        $filter = ['user_id' => $this->userId, 'scenario_id' => $this->scenario];
        if ($this->answerId > 0) {
            $filter['is_ended'] = false;
        }
        $arAttempt = CAftChatBotAttemptTable::getList([
            'order' => ['id' => 'desc'],
            'select' => ['id'],
            'filter' => $filter
        ])->fetchRaw();
        if ($arAttempt['id'] && !$this->isFirstQuestion) {
            $this->attemptId = $arAttempt['id'];
            if ($this->answerId <= 0) {
                $this->updateAttempt();
            }
        } else {
            $this->attemptId = $this->createAttempt();
        }
        return $this;
    }

    /**
     * @return mixed
     */
    private function createAttempt()
    {
        $fields = [
            'user_id' => $this->userId,
            'created_at' => new DateTime(),
            'scenario_id' => $this->scenario
        ];
        return CAftChatBotAttemptTable::add($fields)->getId();
    }

    /**
     * @return false|int|mixed
     */
    private function updateAttempt()
    {
        $scenarioResult = $this->scenarioResult()?:self::$_ids[self::STATUS_WRONG];
        $fields = ['updated_at' => new DateTime(), 'is_ended' => true, 'result' => $scenarioResult];
        if ($this->answerId <= 0) {
            $fields['is_ended'] = false;
        }
        $isUpdate = CAftChatBotAttemptTable::update($this->attemptId, $fields)->isSuccess();
        return $isUpdate ? $scenarioResult : false;
    }

    /**
     * @return mixed
     */
    private function closeAttempt(){
        $scenarioResult = $this->scenarioResult()?:self::$_ids[self::STATUS_WRONG];
        return CAftChatBotAttemptTable::update($this->attemptId, ['updated_at' => new DateTime(), 'is_ended' => true, 'result' => $scenarioResult])->isSuccess();
    }

    /**
     * @return mixed
     */
    private function scenarioResult()
    {
        $lastQuestionResult = CAftChatBotQuestionResultTable::getList([
            'select' => ['*'],
            'filter' => ['user_id' => $this->userId, 'attempt_id' => $this->attemptId],
            'order' => ['id' => 'desc']
        ])->fetchRaw();

        return $lastQuestionResult['answer_correct'];
    }

    /**
     * @return array
     */
    private function getQuestion(): array
    {
        $result = [];
        $emotion = self::$_ids[self::STATUS_ACCEPTABLE];
        if($this->isFirstQuestion){
            $sort = ['sort' => 'asc', 'id' => 'asc'];
            $filter = ['IBLOCK_ID' => CHAT_BOT_IBLOCK_ID, 'SECTION_ID' => $this->scenario];
            $question = CIBlockSection::GetList($sort, $filter, false, ['UF_*'], ['nPageSize' => 1]);
        } else {
            $question = CIBlockSection::GetList([], ['IBLOCK_ID' => CHAT_BOT_IBLOCK_ID, 'ID' => $this->nextQuestionId], false, ['UF_*']);
            $emotion = $this->getAnswerCorrectStatus();
        }
        if($arElement = $question->Fetch()){
            $helperText = $arElement['UF_HELPER_TEXT'] ? $arElement['UF_HELPER_TEXT'] : '';
            $this->nextQuestionId = $this->nextQuestionId?:intval($arElement['ID']);
            $questionText = $arElement['DESCRIPTION'];
            $answers = CIBlockElement::GetList([], ['IBLOCK_ID' => CHAT_BOT_IBLOCK_ID, 'SECTION_ID' => $this->nextQuestionId],false, false, ['ID', 'PREVIEW_TEXT','PROPERTY_NEXT_QUESTION']);
            while($arAnswer = $answers->Fetch()){
                $answerList[$arAnswer['ID']] = [
                    'id' => $arAnswer['ID'],
                    'text' => $arAnswer['PREVIEW_TEXT'],
                    'next' => $arAnswer['PROPERTY_NEXT_QUESTION_VALUE'],
                ];
            }
            $questionTableUpdate = $this->nextQuestionId === $this->questionId ? $this->updateQuestionResult() : $this->addQuestionResult();
            if($questionTableUpdate){
                $result['status'] = true;
                $result['limit'] = true;
                $result['questionId'] = $this->nextQuestionId;
                $result['text'] = $questionText;
                $result['helper'] = $helperText;
                $result['answers'] = array_values($answerList);
                $result['emotion'] = $emotion;
                $result['actions'] = $this->answerId ? $this->getAnswerActions() : [];
                shuffle($result['answers']);
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    private function getAnswerActions(): array
    {
        $actions = [];
        $answer = CIBlockElement::GetList([], ['IBLOCK_ID' => CHAT_BOT_IBLOCK_ID, 'ID' => $this->answerId], false, false, ['PROPERTY_CUSTOMER_ACTION', 'PROPERTY_SALESMAN_ACTION', 'PROPERTY_FINISH_ACTION']);
        if($arAnswer = $answer->Fetch()){
            $arAnswer['PROPERTY_CUSTOMER_ACTION_VALUE']['TEXT'] && $actions['customer'] = $arAnswer['PROPERTY_CUSTOMER_ACTION_VALUE']['TEXT'];
            $arAnswer['PROPERTY_SALESMAN_ACTION_VALUE']['TEXT'] && $actions['salesman'] = $arAnswer['PROPERTY_SALESMAN_ACTION_VALUE']['TEXT'];
            $arAnswer['PROPERTY_FINISH_ACTION_VALUE']['TEXT'] && $actions['finish'] = $arAnswer['PROPERTY_FINISH_ACTION_VALUE']['TEXT'];
        }

        return $actions;
    }

    /**
     * @return mixed
     */
    private function addQuestionResult(){
        $questionFields = [
            'user_id' => $this->userId,
            'attempt_id' => $this->attemptId,
            'question_id' => $this->nextQuestionId,
            'created_at' => new DateTime()
        ];
        return CAftChatBotQuestionResultTable::add($questionFields)->isSuccess();
    }

    /**
     * @return mixed
     */
    private function updateQuestionResult(){
        $row = $this->getQuestionResult();
        return CAftChatBotQuestionResultTable::update(
            $row['id'],
            ['answer_id' => $this->answerId, 'updated_at' => new DateTime(), 'answer_correct' => $this->getAnswerCorrectStatus()]
        )->isSuccess();
    }

    /**
     * @return int
     */
    private function percentagePassed(): int {
        $all = $this->getAllQuestionsCnt();
        $passed = $this->getPassedQuestionsCnt();
        if ($passed >= $all) {
            return 100;
        }
        return floor(($passed/$all)*100);
    }

    /**
     * @return int
     */
    private function getAllQuestionsCnt(): int {
        $sort = ['sort' => 'asc', 'id' => 'asc'];
        $filter = ['IBLOCK_ID' => CHAT_BOT_IBLOCK_ID, 'SECTION_ID' => $this->scenario, 'ACTIVE' => 'Y'];
        $questions = CIBlockSection::GetList($sort, $filter);
        $cnt = 0;
        while($arElement = $questions->Fetch()) {
            $cnt++;
        }
        return $cnt;
    }

    /**
     * @return int
     */
    private function getPassedQuestionsCnt(): int {
        $arSelect = [
            'select' => ['id'],
            'filter' => ['user_id' => $this->userId, 'attempt_id' => $this->attemptId]
        ];
        return CAftChatBotQuestionResultTable::getList($arSelect)->getSelectedRowsCount();
    }

    /**
     * @return mixed
     */
    private function getQuestionResult(){
        return CAftChatBotQuestionResultTable::getList([
            'select' => ['id'],
            'filter' => ['user_id' => $this->userId, 'attempt_id' => $this->attemptId, 'question_id' => $this->questionId]
        ])->fetchRaw();
    }

    /**
     * @return mixed
     */
    private function updateLevel(){
        $row = CAftChatBotLevelsTable::getList([
            'select' => ['id', 'achieved_lvl'],
            'filter' => ['user_id' => $this->userId, 'scenario_id' => $this->scenarioMain]
        ])->fetchRaw();
        $scenarioLvl = $this->scenarioLevel();
        if ($row) {
            if (self::$_lvl[$row['achieved_lvl']]) {
                if (self::$_lvl[$scenarioLvl] < self::$_lvl[$row['achieved_lvl']]) {
                    $scenarioLvl = $row['achieved_lvl'];
                }
            }
            return CAftChatBotLevelsTable::update(
                $row['id'],
                ['achieved_lvl' => $scenarioLvl, 'updated_at' => new DateTime()]
            )->isSuccess();
        } else {
            return CAftChatBotLevelsTable::add([
                'user_id' => $this->userId,
                'scenario_id' => $this->scenarioMain,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
                'achieved_lvl' => $scenarioLvl,
            ])->isSuccess();
        }
    }

    /**
     * @return string
     */
    private function scenarioLevel(){
        $lvl = '';
        $resScenario = CIBlockSection::GetList([], ['IBLOCK_ID' => CHAT_BOT_IBLOCK_ID, 'ID' => $this->scenario], false, ['UF_*']);
        if($arScenario = $resScenario->Fetch()){
            $lvl = $arScenario['UF_LEVEL'];
        }
        if ($lvl) {
            $rsEnum = CUserFieldEnum::GetList(['ID' => 'asc'], ['ID' => $lvl]);
            if($arEnum = $rsEnum->GetNext()) {
                $lvl = $arEnum['XML_ID'];
            }
        }
        return $lvl;
    }

    /**
     * @return int|mixed|null
     */
    private function getAnswerCorrectStatus(){
        $status = null;
        $answer = CIBlockElement::GetList([], ['IBLOCK_ID' => CHAT_BOT_IBLOCK_ID, 'ID' => $this->answerId]);
        if($obAnswer = $answer->GetNextElement()){
            $props = $obAnswer->GetProperties();
            $status = self::$_ids[$props['ANSWER_CORRECT']['VALUE_XML_ID']];
        }
        return $status;
    }

    private function deleteBeforeAnew(){
        return CAftChatBotQuestionResultTable::delete($this->answerRowId)->isSuccess();
    }

    /**
     * @param $userId
     * @param $scenario
     * @return mixed
     */
    public static function getActiveAttempt($userId, $scenario){
        $arAttempt = CAftChatBotAttemptTable::getList([
            'select' => ['id'],
            'filter' => ['user_id' => $userId, 'is_ended' => false, 'scenario_id' => $scenario]
        ])->fetchRaw();

        return $arAttempt['id'];
    }

    /**
     * @param $userId
     * @param $attemptId
     * @return mixed
     */
    public static function getLastQuestion($userId, $attemptId){
        return CAftChatBotQuestionResultTable::getList([
            'select' => ['id', 'question_id'],
            'filter' => ['user_id' => $userId, 'attempt_id' => $attemptId, 'answer_id' => false]
        ])->fetchRaw();
    }

    /**
     * @param $scenario
     * @return int|mixed
     */
    public static function getScenarioAttemptLimit($scenario){
        $limit = self::DEFAULT_ATTEMPT_LIMIT;
        $scenario = CIBlockSection::GetList([], ['IBLOCK_ID' => CHAT_BOT_IBLOCK_ID, 'ID' => $scenario], false, ["ID", "UF_ATTEMPT_COUNT"]);
        if($arScenario = $scenario->Fetch()) $limit = $arScenario['UF_ATTEMPT_COUNT'];

        return $limit;
    }

    public static function getEndedUserAttempts($userId, $scenario){
        return CAftChatBotAttemptTable::getList([
            'select' => ['*'],
            'filter' => ['user_id' => $userId, 'is_ended' => true, 'scenario_id' => $scenario]
        ])->getSelectedRowsCount();
    }

    public static function getGetUserLvl($userId, $scenario){
        $raw = CAftChatBotLevelsTable::getList([
            'select' => ['achieved_lvl'],
            'filter' => ['user_id' => $userId, 'scenario_id' => $scenario]
        ])->fetchRaw();
        if ($raw) {
            return $raw['achieved_lvl'];
        } else {
            return '';
        }
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): ChatBot
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @param int|null $questionId
     * @return $this
     */
    public function setQuestionId(?int $questionId): ChatBot
    {
        $this->questionId = $questionId;
        return $this;
    }

    /**
     * @param int $scenario
     * @return $this
     */
    public function setScenario(int $scenario): ChatBot
    {
        $this->scenario = $scenario;
        return $this;
    }

    /**
     * @param int $scenario
     * @return $this
     */
    public function setScenarioMain(int $scenarioMain): ChatBot
    {
        $this->scenarioMain = $scenarioMain;
        return $this;
    }

    /**
     * @param int $attemptId
     * @return $this
     */
    public function setAttemptId(int $attemptId): ChatBot
    {
        $this->attemptId = $attemptId;
        return $this;
    }

    /**
     * @param int|null $answerId
     * @return $this
     */
    public function setAnswerId(?int $answerId): ChatBot
    {
        $this->answerId = $answerId;
        return $this;
    }

    /**
     * @param bool $isFirstQuestion
     * @return $this
     */
    public function setIsFirstQuestion(bool $isFirstQuestion): ChatBot
    {
        $this->isFirstQuestion = $isFirstQuestion;
        return $this;
    }

    /**
     * @param int|null $nextQuestionId
     * @return $this
     */
    public function setNextQuestionId(?int $nextQuestionId): ChatBot
    {
        $this->nextQuestionId = $nextQuestionId;
        return $this;
    }

    /**
     * @param int $answerRowId
     * @return $this
     */
    public function setAnswerRowId(int $answerRowId): ChatBot
    {
        $this->answerRowId = $answerRowId;
        return $this;
    }
}