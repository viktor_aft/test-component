<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if($_GET['scenario']){
    global $USER;
    $scenario = intval($_GET['scenario']);
    $attemptLimit = ChatBot::getScenarioAttemptLimit($scenario);
    $userAttemptCount = ChatBot::getEndedUserAttempts($USER->GetID(), $scenario);
    $arResult['IS_CHAT_AVAILABLE'] = $attemptLimit > $userAttemptCount;

    if($arResult['IS_CHAT_AVAILABLE']){
        $element = CIBlockSection::GetList([], ['IBLOCK_ID' => CHAT_BOT_IBLOCK_ID, 'ID' => $scenario, 'ACTIVE' => 'Y', 'ACTIVE_DATE=' => 'Y']);
        if($arElement = $element->Fetch()) {
            $arResult['SCENARIO_NAME'] = $arElement['NAME'];
            $arResult['SCENARIO_DESCRIPTION'] = $arElement['DESCRIPTION'];
            $arResult['SCENARIO_ATTEMPT'] = ChatBot::getActiveAttempt($USER->GetID(), $scenario);
        } else {
            LocalRedirect('/obuchenie/');
        }
    }
} else {
    LocalRedirect('/obuchenie/');
}

$this->includeComponentTemplate();
