let chatBlock = document.querySelector('.chatbot__chat');
function sendAnswer(element){
    $(element).addClass('selected');
    let parent = $(element).parent('.chatbot__message--question');
    parent.find('.chatbot__option').attr('disabled', true);
    parent.find('.chatbot__option:not(.selected)').hide();
    let answerId = $(element).data('answer');
    let nextQuestionId = $(element).data('next');
    let questionId = parent.parent('div').data('question');
    if(nextQuestionId){
        nextQuestion(questionId, answerId, nextQuestionId);
    } else {
        finishScenario(questionId, answerId);
    }
}

function nextQuestion(questionId, answerId, nextQuestionId){
    $.ajax({
        url: BX.message('TEMPLATE_PATH')+'/ajax_answer.php',
        data: {questionId:questionId, answerId:answerId, nextQuestionId:nextQuestionId, scenario: $_GET('scenario')},
        method: 'POST',
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            let loader = document.createElement('div');
            loader.classList.add('chatbot__loader');
            loader.innerHTML = '<span></span><span></span><span></span>';
            chatBlock.appendChild(loader);
        },
        success: function(data){
            if(data.status){
                addQuestionBlock(data);
            }
        },
        error: function(){
            console.log('Ошибка скрипта');
        }
    });
    return false;
}

function finishScenario(questionId, answerId){
    $.ajax({
        url: BX.message('TEMPLATE_PATH')+'/ajax_finish.php',
        data: {questionId:questionId, answerId:answerId, scenario: $_GET('scenario')},
        method: 'POST',
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            let loader = document.createElement('div');
            loader.classList.add('chatbot__loader');
            loader.innerHTML = '<span></span><span></span><span></span>';
            chatBlock.appendChild(loader);
        },
        success: function(data){
            chatBlock.removeChild(chatBlock.querySelector('.chatbot__loader'));
            if(data.status){
                if(Object.keys(data.actions).length > 0){
                    //Добавляем экшены
                    addActions(data.actions);
                }
                document.querySelector('.chatbot__result').style.display = 'block';
                let total = parseInt(data.total);
                if(total === 1){
                    document.querySelector('.chatbot__result .chatbot__text-success').style.display = 'block';
                }
                if(total === 2){
                    document.querySelector('.chatbot__result .chatbot__text-acceptable').style.display = 'block';
                }
                if(total === 3){
                    document.querySelector('.chatbot__result .chatbot__text-fail').style.display = 'block';
                }
                document.querySelector('.chatbot__result').scrollIntoView({block: "end", behavior: "smooth"});
            }
        },
        error: function(){
            console.log('Ошибка скрипта');
        }
    });
    return false;
}

$(document).ready(function (){
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $('.chatbot-start').click(function (){
        $.ajax({
            url: BX.message('TEMPLATE_PATH')+'/ajax_start.php',
            data: {scenario: $_GET('scenario')},
            method: 'POST',
            type: 'POST',
            dataType: 'json',
            beforeSend: function (){
                let loader = document.createElement('div');
                loader.classList.add('chatbot__loader');
                loader.innerHTML = '<span></span><span></span><span></span>';
                chatBlock.appendChild(loader);
                $('.chatbot__description').hide();
            },
            success: function (data){
                if(data.status){
                    addQuestionBlock(data);
                }
            }
        })
    });
    $('.chatbot-continue').click(function (){
        $.ajax({
            url: BX.message('TEMPLATE_PATH')+'/ajax_continue.php',
            data: {scenario: $_GET('scenario')},
            method: 'POST',
            type: 'POST',
            dataType: 'json',
            beforeSend: function (){
                let loader = document.createElement('div');
                loader.classList.add('chatbot__loader');
                loader.innerHTML = '<span></span><span></span><span></span>';
                chatBlock.appendChild(loader);
                $('.chatbot__description').hide();
            },
            success: function (data){
                if(data.status){
                    addQuestionBlock(data);
                }
            }
        })
    });
    $('.chatbot-anew').click(function (){
        $.ajax({
            url: BX.message('TEMPLATE_PATH')+'/ajax_anew.php',
            data: {scenario: $_GET('scenario')},
            method: 'POST',
            type: 'POST',
            dataType: 'json',
            beforeSend: function (){
                let loader = document.createElement('div');
                loader.classList.add('chatbot__loader');
                loader.innerHTML = '<span></span><span></span><span></span>';
                chatBlock.appendChild(loader);
                $('.chatbot__description').hide();
            },
            success: function (data){
                if(data.status){
                    if(data.limit){
                        addQuestionBlock(data);
                    } else {
                        $('.chatbot__wrap').remove();
                        $('.chatbot__description').empty();
                        $('.chatbot__description').html('Вы исчерпали лимит попыток');
                        $('.chatbot__description').show();
                    }
                }
            }
        })
    });
    $('.chatbot-reload').click(function (){
        document.location.reload();
    })
})

function addQuestionBlock(data){
    //Удаляем лоадер
    chatBlock.removeChild(chatBlock.querySelector('.chatbot__loader'));
    //Проверяем наличие экшенов
    if(Object.keys(data.actions).length > 0){
        //Добавляем экшены
        addActions(data.actions);
    }
    //Создаем блок для вопроса и ответов
    let answerBlock = document.createElement('div');
    answerBlock.dataset.question = data.questionId;
    //Создаем блок с вопросом
    let questionBlock = document.createElement('div');
    questionBlock.classList.add('chatbot__message', 'chatbot__message--client');
    let questionBlob = document.createElement('div');
    questionBlob.classList.add('chatbot__blob');
    questionBlob.innerHTML = data.text;
    questionBlock.appendChild(questionBlob);
    //Создаем блок с ответами
    let answersBlock = document.createElement('div');
    answersBlock.classList.add('chatbot__message', 'chatbot__message--user', 'chatbot__message--question');
    let answersBlob = document.createElement('p');
    answersBlob.classList.add('chatbot__caption');
    answersBlob.innerHTML = "Ваш ответ";
    answersBlock.appendChild(answersBlob);
    data.answers.forEach(function(item){
        let answer = document.createElement('button');
        answer.classList.add('chatbot__blob', 'chatbot__option');
        answer.dataset.next = item.next;
        answer.dataset.answer = item.id;
        answer.type = "button";
        answer.addEventListener('click', function() {sendAnswer(this)}, false);
        answer.innerHTML = item.text;
        answersBlock.appendChild(answer);
    });
    //добавляем все в блок вопроса
    answerBlock.appendChild(questionBlock);
    answerBlock.appendChild(answersBlock);
    chatBlock.appendChild(answerBlock);
}

function addActions(actions){
    let lastScenarioPart = chatBlock.lastElementChild;
    if(actions.customer){
        let actionBlock = document.createElement('div');
        actionBlock.classList.add('chatbot__message', 'chatbot__message--client');
        let actionBlob = document.createElement('div');
        actionBlob.classList.add('chatbot__blob');
        actionBlob.innerHTML = actions.customer;
        actionBlock.appendChild(actionBlob);
        lastScenarioPart.appendChild(actionBlock);
    }
    if(actions.salesman){
        let actionBlock = document.createElement('div');
        actionBlock.classList.add('chatbot__message', 'chatbot__message--user');
        let actionBlob = document.createElement('p');
        actionBlob.classList.add('chatbot__text');
        actionBlob.innerHTML = actions.salesman;
        actionBlock.appendChild(actionBlob);
        lastScenarioPart.appendChild(actionBlock);
    }
    if(actions.finish){
        let actionBlock = document.createElement('div');
        actionBlock.classList.add('chatbot__message', 'chatbot__message--user');
        let actionBlob = document.createElement('p');
        actionBlob.classList.add('chatbot__text');
        actionBlob.innerHTML = actions.finish;
        actionBlock.appendChild(actionBlob);
        lastScenarioPart.appendChild(actionBlock);
    }
}

function $_GET(key) {
    var p = window.location.search;
    p = p.match(new RegExp(key + '=([^&=]+)'));
    return p ? p[1] : false;
}
