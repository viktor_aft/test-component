<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
global $USER;
$checkToken = checkToken($USER->GetID());
if(!$checkToken){
    echo json_encode(['status'=>false, 'result'=>'Обновление не удалось']);
    die();
}
$scenario = intval($_POST['scenario']);

$chatBot = new ChatBot();
$result = $chatBot->setIsFirstQuestion(true)->setUserId($USER->GetID())->setScenario($scenario)->actionGetQuestion();

echo json_encode($result);