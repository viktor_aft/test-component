<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
global $USER;
$checkToken = checkToken($USER->GetID());
if(!$checkToken){
    echo json_encode(['status'=>false, 'result'=>'Обновление не удалось']);
    die();
}
$scenario = intval($_POST['scenario']);
$attempt = ChatBot::getActiveAttempt($USER->GetID(), $scenario);
$arQuestion = ChatBot::getLastQuestion($USER->GetID(), $attempt);

$chatBot = new ChatBot();
$result = $chatBot->setNextQuestionId($arQuestion['question_id'])->setScenario($scenario)->setUserId($USER->GetID())->actionGetQuestion();

echo json_encode($result);
