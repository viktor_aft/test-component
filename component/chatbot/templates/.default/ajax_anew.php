<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
global $USER;
$checkToken = checkToken($USER->GetID());
if(!$checkToken){
    echo json_encode(['status'=>false, 'result'=>'Обновление не удалось']);
    die();
}

$scenario = intval($_POST['scenario']);
$attempt = ChatBot::getActiveAttempt($USER->GetID(), $scenario);
$arQuestion = ChatBot::getLastQuestion($USER->GetID(), $attempt);

$result = [];
$chatBot = new ChatBot();
$isAttemptClose = $chatBot->setUserId($USER->GetID())->setAttemptId($attempt)->setScenario($scenario)->setAnswerRowId($arQuestion['id'])->actionAnew();
if($isAttemptClose){
    $attemptLimit = ChatBot::getScenarioAttemptLimit($scenario);
    $userAttemptCount = ChatBot::getEndedUserAttempts($USER->GetID(), $scenario);
    if($attemptLimit > $userAttemptCount){
        $result = $chatBot->setIsFirstQuestion(true)->setScenario($scenario)->actionGetQuestion();

    } else {
        $result = ['status' => true, 'limit' => false];
    }
}
echo json_encode($result);
