<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
global $USER;
$checkToken = checkToken($USER->GetID());
if(!$checkToken){
    echo json_encode(['status'=>false, 'result'=>'Обновление не удалось']);
    die();
}

$questionId = intval($_POST['questionId']);
$answerId = intval($_POST['answerId']);
$nextQuestionId = intval($_POST['nextQuestionId']);
$scenario = intval($_POST['scenario']);

$chatBot = new ChatBot();
$result = $chatBot
    ->setUserId($USER->GetID())
    ->setAnswerId($answerId)
    ->setQuestionId($questionId)
    ->setNextQuestionId($nextQuestionId)
    ->setScenario($scenario)
    ->actionSetAnswer();

echo json_encode($result);