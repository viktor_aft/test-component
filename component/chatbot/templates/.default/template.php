<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<section class="section chatbot">
    <div class="container">
        <div class="chatbot__top">
            <a href="/obuchenie/" class="chatbot__back"><span>Назад</span></a>
            <h1 class="section__title chatbot__title"><?=$arResult['SCENARIO_NAME']?></h1>
        </div>
        <?if($arResult['IS_CHAT_AVAILABLE']):?>
            <div class="chatbot__description">
                <?=$arResult['SCENARIO_DESCRIPTION']?>
                <?if($arResult['SCENARIO_ATTEMPT']):?>
                    <button type="button" class="btn chatbot__btn chatbot-continue">Продолжить сценарий</button>
                    <button type="button" class="btn chatbot__btn chatbot-anew">Начать сценарий заново</button>
                <?else:?>
                    <button type="button" class="btn chatbot__btn chatbot-start">Начать сценарий</button>
                <?endif;?>
            </div>
            <div class="chatbot__wrap">
                <div class="chatbot__chat"></div>
                <div class="chatbot__result" style="display: none;">
                    <p class="chatbot__caption">Результат</p>
                    <p class="chatbot__text chatbot__text-success">Задание выполнено успешно</p>
                    <p class="chatbot__text chatbot__text-acceptable">Задание выполнено не полностью</p>
                    <p class="chatbot__text chatbot__text-fail">Задание не выполнено</p>
                    <button type="button" class="btn chatbot__btn chatbot-reload">Пройти тест еще раз</button>
                </div>
            </div>
        <?else:?>
            <div class="chatbot__description">Вы исчерпали лимит попыток</div>
        <?endif;?>
    </div>
</section>
<script>
    BX.message({
        TEMPLATE_PATH: '<?=$this->GetFolder()?>'
    });
</script>