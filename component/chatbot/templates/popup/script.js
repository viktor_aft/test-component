let answers = {
    1: {
        message: 'Большой такой телевизор.'
    },
    2: {
        message: 'C 2х метров.'
    },
    3: {
        message: 'Телевизовы отличные, но стоят намного дороже, чем я планировал.',
        action: 'Вы рассказываете покупателю условия рассрочки, используя методы борьбы с возражнениями "Сопративление расходам"',
        message2: 'Очень интересно, мне подходит вариант OLED B1, думаю, я вполне потяну'
    },
}
let closePopup = document.querySelector('.chatbot__back');
if (closePopup) {
    closePopup.addEventListener('click', function () {
        document.querySelector('body').classList.remove('no-scroll');
        document.querySelector('.chatbot').style.display = 'none';
    });
}

let chatBlock = document.querySelector('.chatbot__chat');
let options = document.querySelectorAll('.chatbot__option');
if (options) {
    for (let i = 0; i < options.length; i++) {
        options[i].addEventListener('click', function () {
            onChooseOption(this);
        });
    }
}
function onChooseOption(evt) {
    evt.classList.add('chatbot__option--active');
    let answerId = evt.dataset.answer;
    let text = evt.innerText;
    let parent = evt.parentElement;
    let siblingOptions = parent.querySelectorAll('.chatbot__option');
    for (let i = 0; i < siblingOptions.length; i++) {
        siblingOptions[i].disabled = true;
    }
    //создать сообщение из выбранного и скрыть выбор
    createAnswer(text, 'user');
    parent.classList.add('chatbot__message--hidden');
    // лоадер
    let loader = document.createElement('div');
    loader.classList.add('chatbot__loader');
    loader.innerHTML = '<span></span><span></span><span></span>';
    chatBlock.appendChild(loader);

    setTimeout(function () {
        //удалить лоадер
        chatBlock.removeChild(chatBlock.querySelector('.chatbot__loader'));
        // добавить ответ
        let clientAnswer = answers[answerId];
        for (let key in clientAnswer) {
            if(key.indexOf('message') !== -1) {
                createAnswer(answers[answerId][key], 'client')
            } else {
                createActionText(answers[answerId][key]);
            }
        }
        if (answerId == 3) {
            document.querySelector('.chatbot__result').style.display = 'block';
            document.querySelector('.chatbot__result').scrollIntoView({block: "end", behavior: "smooth"});
        }
    }, 1000);
}
function createAnswer(text, type) {
    let typeClass = type === 'client' ? 'chatbot__message--client' : 'chatbot__message--user';
    let answerBlock = document.createElement('div');
    answerBlock.classList.add('chatbot__message');
    answerBlock.classList.add(typeClass);
    let answerBlob = document.createElement('div');
    answerBlob.classList.add('chatbot__blob');
    answerBlob.textContent = text;
    answerBlock.appendChild(answerBlob);
    chatBlock.appendChild(answerBlock);
}
function createActionText(text) {
    let answerBlock = document.createElement('div');
    answerBlock.classList.add('chatbot__message');
    answerBlock.classList.add('chatbot__message--user');
    let answerBlob = document.createElement('p');
    answerBlob.classList.add('chatbot__text');
    answerBlob.textContent = text;
    answerBlock.appendChild(answerBlob);
    chatBlock.appendChild(answerBlock);
}
let chatBtn = document.getElementById('openChat');
if (chatBtn) {
    chatBtn.addEventListener('click', function () {
        document.querySelector('body').classList.add('no-scroll');
        document.querySelector('.chatbot').style.display = 'block';
    })
}