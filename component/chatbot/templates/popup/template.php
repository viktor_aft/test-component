<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="chatbot">
    <div class="chatbot__bg"></div>
    <div class="chatbot__cover">
        <div class="container">
            <div class="chatbot__top">
                <h1 class="section__title chatbot__title"><?=$arResult['SCENARIO_NAME']?></h1>
                <button type="button" class="chatbot__back"><span>Назад</span></button>
            </div>
            <div class="chatbot__wrap">
                <div class="chatbot__chat">
                    <p class="chatbot__caption chatbot__caption--client">Покупатель</p>
                    <div class="chatbot__message chatbot__message--client">
                        <div class="chatbot__blob">
                            Я уже долгое время пользуюсь телевизором Sumsung и чейчас хотелось бы что-то более новое и не очень дорогое
                        </div>
                    </div>
                    <div class="chatbot__message chatbot__message--user">
                        <div class="chatbot__blob">
                            Что для Вас наиболее важно при использовании вашего телевизора?
                        </div>
                    </div>
                    <div class="chatbot__message chatbot__message--client">
                        <div class="chatbot__blob">
                            Я люблю смотреть онлайн кинотеатры на моем телвизовы и играть в аидеоигры на компьютере и приставке
                        </div>
                    </div>
                    <div class="chatbot__message chatbot__message--user chatbot__message--question">
                        <p class="chatbot__caption">Ваш ответ</p>
                        <button class="chatbot__blob chatbot__option" type="button" data-answer="1">Подскажите, а какой размер у вашего старого телевизора?</button>
                        <button class="chatbot__blob chatbot__option" type="button" data-answer="2">Отлично. Подскажите пожалуйста, с какого расстояния вы обычно смотрите Ваш старый телевизор?</button>
                        <button class="chatbot__blob chatbot__option" type="button" data-answer="3">Тогда Вам подойдет OLED 65 G1. Давайте я Вам его покажу.</button>
                    </div>
                </div>
                <div class="chatbot__result" style="display: none;">
                    <p class="chatbot__caption">Результат</p>
                    <p class="chatbot__text">Поздравляем!<br> покупатель оформляет покупку в рассрочку. Продажа прошла успешно, покупатель остался очень доволен своим новым телевизором и готов к дальнейшему контакту</p>
                    <button type="button" class="btn chatbot__btn">Пройти тест еще раз</button>
                </div>
            </div>
        </div>
    </div>
</div>
