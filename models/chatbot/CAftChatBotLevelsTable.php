<?php

use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\DatetimeField;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\Entity\DataManager;

class CAftChatBotLevelsTable extends DataManager
{

    public static function getTableName(): string
    {
        return 'aft_chat_bot_user_levels';
    }

    public static function getMap(): array
    {
        return [
            (new IntegerField('id'))->configurePrimary(true)->configureAutocomplete(true),
            (new IntegerField('user_id'))->configureRequired(true),
            (new IntegerField('scenario_id'))->configureRequired(true),
            (new DatetimeField('created_at')),
            (new DatetimeField('updated_at')),
            (new StringField('achieved_lvl'))->configureRequired(true),
        ];

    }
}