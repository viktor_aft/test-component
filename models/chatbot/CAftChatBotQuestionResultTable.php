<?php

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\DatetimeField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\SystemException;

class CAftChatBotQuestionResultTable extends DataManager
{
    public static function getTableName(): string
    {
        return 'aft_chat_bot_question_result';
    }

    /**
     * @throws SystemException
     * @throws ArgumentException
     */
    public static function getMap(): array
    {
        return [
            (new IntegerField('id'))->configurePrimary(true)->configureAutocomplete(true),
            (new IntegerField('attempt_id'))->configureRequired(true),
            (new Reference('attempt', CAftChatBotQuestionResultTable::class, Join::on('this.attempt_id', 'ref.id')))->configureJoinType('inner'),
            (new IntegerField('user_id'))->configureRequired(false),
            (new DatetimeField('created_at')),
            (new DatetimeField('updated_at')),
            (new IntegerField('question_id'))->configureRequired(true),
            (new IntegerField('answer_id'))->configureDefaultValue(null),
            (new IntegerField('answer_correct')),
        ];
    }
}