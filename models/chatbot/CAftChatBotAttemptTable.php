<?php

use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\BooleanField;
use Bitrix\Main\ORM\Fields\DatetimeField;
use Bitrix\Main\Entity\DataManager;

class CAftChatBotAttemptTable extends DataManager
{

    public static function getTableName(): string
    {
        return 'aft_chat_bot_attempt';
    }

    public static function getMap(): array
    {
        return [
            (new IntegerField('id'))->configurePrimary(true)->configureAutocomplete(true),
            (new IntegerField('user_id'))->configureRequired(true),
            (new IntegerField('scenario_id'))->configureRequired(true),
            (new DatetimeField('created_at')),
            (new DatetimeField('updated_at')),
            (new BooleanField('is_ended'))->configureRequired(false)->configureDefaultValue(false),
            (new IntegerField('result')),
        ];

    }
}